// 	CRUD operation

/*
	heart of backend 

	create read update delete 

	mongodb deals with object as its structure for documents we can easily create them by providing objects into our methods
*/


// create: inserting decuments 

//insert one
/*
syntax
	db.collectionName.insertOne({})

js assignning value 
	object.object.method({object})
*/
db.user.insertOne({
	firstName: "jane",
	lastName: "doe",
	age: 23,
	contact{
		phone: "5677556",
		email: "werw@email.com"
	},
	courses: ['css', 'javascript', 'python'],
	department: "none"
});


//insert many
/*
	syntax
	db.users.insertMany([
		{objectA:},
		{objectB:},
	])
*/


db.user.insertMany([
	{
	firstName: "stephen",
	lastName: "hawking",
	age: 76,
	contact:{
		phone: "99077556",
		email: "step@email.com"
	},
	courses: ['php', 'react', 'python'],
	department: "none"
	},
	{
	firstName: "neil",
	lastName: "armstrong",
	age: 90,
	contact:{
		phone: "787878",
		email: "neil@email.com"
	},
	courses: ['react', 'laravel', 'sass'],
	department: "none"
	}
]);



//read: finding documents 

/*
	find all

	syntax
		db.collection.find()
*/

db.user.find();


//finding users with single arguments
/*
	syntax
	db.collectionName.find({field: value})
*/ 
db.user.find({firstName:"stephen"})

//match 0
db.user.find({firstName:"stephen", age: 20});

//match 1
db.user.find({firstName:"stephen", age: 20});

//update documents

db.user.insertOne({
	firstName: "jane",
	lastName: "doe",
	age: 23,
	contact{
		phone: "5677556",
		email: "werw@email.com"
	},
	courses: ['css', 'javascript', 'python'],
	department: "none"
});

//update one

/*
	syntax
		db.collectionName.updateOne({criteria}, {$set: (field: value)})
*/

db.user.updateOne(
	{firstName: "jane"},
	{
		$set:{
			firstName: "jane",
			lastName: "gates",
			age: 65,
			contact:{
				phone: "5677556",
				email: "werw@email.com"
			},
			courses: ['aws', 'google cloud', 'azure'],
			department: "infrastracture",
			status: "active"
			}
	}
);
db.user.find({firstName:"jane"});


//UPDATE many

db.user.updateMany(
	{department:"none"},
	{
		$set: {
			department: "HR"
		}
	}
);
db.user.find().pretty();

//replace one
/*
	can be used ifreplacing the whole document is necessary

	syntax:
		db.collectionName.replace({criteria}, {{field: value}} )
*/
db.users.replaceOne(
	{ lastName: "gates" },
        {
			firstName: "Bill",
			lastName: "Clinton"
	}
)
db.user.find({firstName: "Bill"});


//deleting documents
db.user.insert({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact:{
		phone: "000000",
		email: "test@email.com"
	},
	courses: [],
	department: "none"
})
db.user.find({firstName:"test"});

//deleting a single document
/*
syntax
	db.collectionName.deleteOne({criteria})
*/
db.user.deleteOne({firstName: "test"});
db.user.find({firstName: "test"});

//delete many
db.user.deleteMany({
	courses:[]
})
db.user.find();

//delete all
//db.user.delete()
//db.user.deleteMany({});
/*
	becareful when using the "deleteMany" method. If no search criteria is provided  it will delete all documents
	do not use db.collection.deleteMany()

	syntax
		db.collectionName.deleteMany({criteria})
*/


//advanced query 

//query an embeded document

db.user.find({
	contact:{
		phone: "5677556",
		email: "werw@email.com"
			}
});

//find the document w/ the email "werw@email.com"
//query on nested field
db.user.find({
	"contact.email":"werw@email.com"
});

//querying an array with exact elements
db.user.find({ courses : [ 
        "react", 
        "laravel", 
        "sass"
    ]});

//oks kahit di exact
db.user.find({ courses : {$all:[ 
        "laravel", 
        "react", 
        "sass"
    ]}});


// make an array query

db.user.insert({
	namearr:[
		{namea:"juan"},
		{nameb:"tamad"}
		]
	
})


//find 
db.user.find({
	namearr:{
		namea:"juan"
	}
})








